% Software deployment and runtime adaptation (Ansible and Docker Swarm)

# Introduction

You will learn how to use Ansible to deploy and configure software on multiple 
remote hosts, and how to adapt an application in a Docker Swarm cluster at runtime. 
In this assignment, you will create several playbooks and use Ansible to set up and 
configure a Docker swarm cluster on top of a set of VMs. You will also use the deployed
Docker swarm cluster to practice the service autoscaling. 


# Reporting and Assessment

## Reporting

At the end of this assignment, you (individually) should:

  * Create playbooks, using the information from this tutorial, that will: 
    + Install and configure a Docker swarm cluster;
   
  * Write a short report (max 4 pages), which should include the created playbooks in YAML format (git link or zip archive)
   
  * Report the results of the following tasks, e.g., screenshots for each of the steps and/or performance measurement, etc. 
    + Install and configure a Docker swarm cluster; 
    + Stress test a simple Apache server with one and 4 instances and report the results;
    
  * Answer all self-study questions

---

 **IMPORTANT**

 Do not add your playbooks to your PDF report! Name your playbook 'configure-cluster.yml'

---

## Self-study questions

1. Discuss how Ansible can be used during the DevOps lifecycle, e.g., which stages? What are the advantages, alternatives of Ansible?
2. Can you use Ansible with Azure? Is it essential to use Azure DevOps with Ansible?

## Assessment  

If your Ansible files perform the steps defined above, and you have performed the stress 
test you will receive 60%; your report will determine the rest of 40%.

To be given a grade, you must submit the following:

 * Written report (see above for details)
 * Ansible Playbooks (git link or zip archive)
 


# Background

## Technologies Overview 
* Ansible: Provisioning, configuration management, and application-deployment , https://www.ansible.com/
* Docker Swarm: Container-orchestration, https://docs.docker.com/engine/swarm//.

Ansible uses the following terms:

 * **Controller Machine**: the machine where Ansible is installed. It manages the execution of the Playbook. It can be installed on your laptop or any machine on the internet
 * **Inventory**: provides a complete list of all the target machines on which various modules are run by making an ssh connection and install the necessary software’s
 * **Playbook**: consists of steps that the control machine will perform on the servers defined in the inventory file
 * **Task**: a block that defines a single procedure to be executed, e.g., install a package
 * **Module**: the main building blocks of Ansible and are reusable scripts that are used by Ansible playbooks. Ansible comes with many reusable modules. These include functionality for controlling services, software package installation, working with files and directories, etc.
 * **Role**: a way for organizing playbooks and other files to facilitate sharing and reusing portions of a provisioning
 * **Facts**: global variables containing information about the system, like network interfaces or operating system
 * **Handlers**: used to trigger service status changes, like restarting or stopping a service


# Prepare your Development Environment 

## Install Ansible 

Start one t2.micro Amazon Linux ami-0c293f3f676ec4f90. Log in the newly created VM run:

```bash
sudo amazon-linux-extras install ansible2
```

Check the installation: 

```bash
ansible --version
```

Copy the ssh private key from the sandbox to the newly created VM and save it at /home/ec2-user/labuser.pem

[//]: # (For Linux and macOS follow these instructions:)

[//]: # (* https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

[//]: # (For windows you can use subsystem:)

[//]: # (* https://docs.ansible.com/ansible/latest/user_guide/windows_faq.html#can-ansible-run-on-windows)

[//]: # (Or Cygwin:)

[//]: # (* https://everythingshouldbevirtual.com/automation/ansible-using-ansible-on-windows-via-cygwin/)

[//]: # (You can always use a VM from EC2 or on your machine. Alternatively you may also use )

[//]: # (VirtualBox: [https://www.virtualbox.org/wiki/Downloads]&#40;https://www.virtualbox.org/wiki/Downloads&#41;)


# Introduction to Ansible 

Make sure Ansible is working by executing the following command:
```
ansible all --inventory "localhost," --module-name debug --args "msg='Hello'"
```

Here is a break-down  of Ansible the command: 
* **all**: this means do run the module on all machines that are listed in the “inventory” file, which is the next part of the command
* **--inventory "localhost,"**: The inventory is where all details of the machines are listed such as IP addresses, usernames, etc. In this case, we only use our local computer. This may also be a file 
* **--module-name debug**: Specify which module to use. In this case the “debug” module, prints statements during execution and can be useful for debugging variables 
* **--args "msg='Hello'"**: Part of the debug module. In this case ‘Hello’ is the customized massage that is printed. If omitted, prints a generic message.


## Controlling Hosts 
Start 2 t2.small Amazon Linux ami-0c293f3f676ec4f90
VMs using EC2. Make sure that port 22 is open (see security groups) 

Create a text file named 'aws_hosts1' that looks like this:
[aws_hosts1](https://gitlab-fnwi.uva.nl/skoulou1/devops-material/-/raw/main/ansible/aws_hosts1)



---

 **IMPORTANT**

 Don't forget to change the hostnames in the file with the names of your VMs 

---


Note that the **ansible_ssh_private_key_file** has to correspond to the key that 
you use to ssh to the provisioned VMs.

You will notice that this file has some heading in brackets **[aws]**  and **[aws:vars]**. 
The first heading in brackets is a group name. You can have more than one group name, 
which is used to classify systems and decide what systems you are controlling at what times and for what purpose. So, in this case, we only have a specified [aws] group. 
To assign variables to hosts, you can use the [aws:vars]  group variables. In this case, we set the VMs username and the location of the key. For more information on 
inventories see here: https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html

Next, run:
```
ansible aws --inventory aws_hosts1 -m setup
```

The setup module will gather information about the target machines. 

The output should look like this:

```json
ec2-xx-xx-xx-xx.compute-1.amazonaws.com | SUCCESS => {
    "ansible_facts": {
        "ansible_all_ipv4_addresses": [
            "172.31.86.189"
        ],
        "ansible_all_ipv6_addresses": [
            "fe80::104f:5aff:fedb:8ee"
        ],
        "ansible_apparmor": {
            "status": "disabled"
        },
        "ansible_architecture": "x86_64",
        "ansible_bios_date": "08/24/2006",
        "ansible_bios_version": "4.2.amazon",
        "ansible_cmdline": {
            "console": "ttyS0",
            "nvme_core.io_timeout": "4294967295",
            "root": "LABEL=/",
            "selinux": "0"
        },
......
}
```

### Using Playbooks

Ansible Playbooks are like a to-do list for Ansible that contains a list of
tasks. They are written in YAML format and run sequentially. 

### Playbook Structure
Each playbook is an aggregation of one or more plays, and there can be more than one play inside the playbook. A play maps a set of instructions defined against a 
particular host.


### Create a Playbook

Start 2 t2.micro Ubuntu Server 18.04 LTS (HVM), SSD Volume Type - ami-0ac019f4fcb7cb7e6  
VMs using EC2 and make sure that ports 22 and 80 are open.  

After all VMs have started get their public IP (DNS) and add them and update the **aws_hosts1**.

Create a playbook that will install in both VMs java:
[playbook_example1.yml](https://gitlab-fnwi.uva.nl/skoulou1/devops-material/-/raw/main/ansible/playbook_example1.yml)


Execute the playbook: 
```
ansible-playbook -i aws_hosts1 playbook_example1.yml
```


The output should look like this:
```bash
[WARNING]: Platform linux on host ec2-34-238-41-120.compute-1.amazonaws.com is using the discovered Python interpreter at /usr/bin/python,
but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
changed: [ec2-34-238-41-120.compute-1.amazonaws.com]
[WARNING]: Platform linux on host ec2-54-211-44-157.compute-1.amazonaws.com is using the discovered Python interpreter at /usr/bin/python,
but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
changed: [ec2-54-211-44-157.compute-1.amazonaws.com]

PLAY RECAP ***********************************************************************************************************************************
ec2-34-238-41-120.compute-1.amazonaws.com : ok=1    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
ec2-54-211-44-157.compute-1.amazonaws.com : ok=1    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
```


#### Execute plays on different hosts
If we want to execute different plays on different hosts, for example, we need to install Apache on one 
host and MySQL on another we need to specify that in the playbook. 

Modify the following inventory with your :
[aws_hosts2](https://gitlab-fnwi.uva.nl/skoulou1/devops-material/-/raw/main/ansible/aws_hosts2)

Change the hostnames in the file with the names of your VMs.

We will use the following playbook: 
[playbook_example2.yml](https://gitlab-fnwi.uva.nl/skoulou1/devops-material/-/raw/main/ansible/playbook_example2.yml)


And execute:
```
ansible-playbook -i aws_hosts1 playbook_example2.yml
```

If we open a browser to **[web-server]** we should see Apache running. 

If you cannot connect check the VMs' inbound rules in its Security Groups. 

Make sure to stop the server:
Get the playbook to stop the server:
[playbook_example2-1.yml](https://gitlab-fnwi.uva.nl/skoulou1/devops-material/-/raw/main/ansible/playbook_example2-1.yml)

and run: 
```bash
ansible-playbook -i aws_hosts2 playbook_example2-1.yml
```


#### Pass Variables Between Plays 
Sometimes it is necessary to pass variables between plays. Consider the following 
playbook:
[playbook_example3.yml](https://gitlab-fnwi.uva.nl/skoulou1/devops-material/-/raw/main/ansible/playbook_example3.yml)

if we execute:
```bash
ansible-playbook -i aws_hosts2 playbook_example3.yml
```

The play with the ‘db’ hosts will fail. 


```bash
[ec2-user@ip-172-31-93-236 ~]$ ansible-playbook -i aws_hosts2 playbook_example3.yml 
[WARNING]: Invalid characters were found in group names but not replaced, use -vvvv to see details

PLAY [web-server] ****************************************************************************************************************************

TASK [Gathering Facts] ***********************************************************************************************************************
[WARNING]: Platform linux on host ec2-54-211-44-157.compute-1.amazonaws.com is using the discovered Python interpreter at /usr/bin/python,
but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
ok: [ec2-54-211-44-157.compute-1.amazonaws.com]

TASK [generate secret] ***********************************************************************************************************************
changed: [ec2-54-211-44-157.compute-1.amazonaws.com]

TASK [debug] *********************************************************************************************************************************
ok: [ec2-54-211-44-157.compute-1.amazonaws.com] => {
    "msg": "Secret password is OTlmYzcxNzhlMjg4YmJjMTg5MTFlZDZh"
}

PLAY [db] ************************************************************************************************************************************

TASK [Gathering Facts] ***********************************************************************************************************************
[WARNING]: Platform linux on host ec2-34-238-41-120.compute-1.amazonaws.com is using the discovered Python interpreter at /usr/bin/python,
but future installation of another Python interpreter could change this. See
https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
ok: [ec2-34-238-41-120.compute-1.amazonaws.com]

TASK [print paswd] ***************************************************************************************************************************
fatal: [ec2-34-238-41-120.compute-1.amazonaws.com]: FAILED! => {"msg": "The task includes an option with an undefined variable. The error was: 'command_output' is undefined\n\nThe error appears to be in '/home/ec2-user/playbook_example3.yml': line 14, column 7, but may\nbe elsewhere in the file depending on the exact syntax problem.\n\nThe offending line appears to be:\n\n  tasks:\n    - name: print paswd\n      ^ here\n"}

PLAY RECAP ***********************************************************************************************************************************
ec2-34-238-41-120.compute-1.amazonaws.com : ok=1    changed=0    unreachable=0    failed=1    skipped=0    rescued=0    ignored=0   
ec2-54-211-44-157.compute-1.amazonaws.com : ok=3    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0  
```


The variable stored on one play is not visible on the next. Instead, we need to use 'hostvars':
[playbook_example4.yml](https://gitlab-fnwi.uva.nl/skoulou1/devops-material/-/raw/main/ansible/playbook_example4.yml)

If we execute this playbook:
```bash
ansible-playbook -i aws_hosts2 playbook_example4.yml
```
we’ll see that the variable is now available to the 'db' hosts as well. 
More information about variables and 'hostvars' can be found here:  https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#accessing-information-about-other-hosts-with-magic-variables.



## Exercises

### Ansible
Create a playbook that will install and configure a Docker swarm cluster

Use the following inventory: [ansible_cluster_hosts](https://gitlab-fnwi.uva.nl/skoulou1/devops-material/-/raw/main/ansible/ansible_cluster_hosts)
And the following playbook: [configure-cluster.yml](https://gitlab-fnwi.uva.nl/skoulou1/devops-material/-/raw/main/ansible/configure-cluster.yml)

#### Process 
Fill in the tasks in the playbook provided:

  * Go to line 44 and save the output of the 'join_cmd' so it is accessible by the workers
  * Go to line 55 and on the workers execute the command to join the cluster 
  * Advanced (optional) extend the playbook to:
    + be able to add new workers on the existing cluster 
    + create one or more worker VMs
    + add them to the running cluster  


    

When you have your playbooks, ready execute the Docker swarm setup playbook.  Note if 
you have problems with initializing the cluster on the master node  or joining the 
cluster on the workers you may need to open all TCP traffic between the VMs of the cluster.

### Docker swarm scale benchmark 
Log in the master node and deploy an Apache server:

```bash
docker service create -p 80:80 --name webservice --replicas 1 httpd
```


Apache is running on port 80.
Test it by opening your browser at http://MATER-IP. 

Now we can benchmark Apache. To do that, install apache2-utils:
```
sudo yum install httpd
```

Run the benchmark:
```
ab -n 50000 -r -c 500 http://MATER-IP/
```
Use the output for your report. The table you are interested in looks like this:
```
Concurrency Level:      XXXX
Time taken for tests:   XXXXX seconds
Complete requests:      XXXX
Failed requests:        XXX
   (Connect: 0, Receive: XXX, Length: XXX, Exceptions: XXXX)
Total transferred:      XXXX bytes
HTML transferred:       XXXX bytes
Requests per second:    XXX [#/sec] (mean)
Time per request:       XXXX [ms] (mean)
Time per request:       XXXX [ms] (mean, across all concurrent requests)
Transfer rate:          XXX [Kbytes/sec] received
```


Add 4 more instances:
```bash
docker service scale webservice=4
```

Run the benchmark again and record the results:
ab -n 50000 -r -c 500 http://MATER-IP:PORT/

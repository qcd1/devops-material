Instuctions from https://docs.gitlab.com/runner/install/linux-manually.html and https://docs.gitlab.com/runner/register/

1. Start an Ubuntu 20.04 VM
2. Log in
3. Install Docker and newman:
```shell
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get update && sudo apt-get install -y ca-certificates curl gnupg lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update && sudo apt-get install -y docker-ce docker-ce-cli containerd.io npm
sudo npm install -g newman
```
4. Install Runner:
```shell
curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb"
sudo dpkg -i gitlab-runner_amd64.deb
```

5. Register Runner:
Use the URL and token https://gitlab-fnwi.uva.nl/<USER>/<PROJET>/-/settings/ci_cd#js-runners-settings
```shell
sudo gitlab-runner register
```
For executor select shell

7. Add to suders 
```shell
sudo usermod -a -G sudo gitlab-runner
```
8. Allow sudo without password. Open suders file:
```shell
sudo visudo
```
9. Add the following to the bottom of the file:
```shell
gitlab-runner ALL=(ALL) NOPASSWD: ALL
```

6. Start:
```shell
sudo gitlab-runner start
```
---
**NOTE**

You may want to stop your VM to save some credits 

---
